﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickMovement : MonoBehaviour {

	public Vector3 targetPosition;

	public GameObject playerStart; // Find position of AI in inspector

	public void Start ()
	{
		// Set to AI position so character is still at load
		transform.position = playerStart.transform.position;
	}

	public void Update ()
	{
		if (Input.GetMouseButtonDown(0))
		{
			if (EventSystem.current.IsPointerOverGameObject()) return; // If mouse is over UI game object, don't register movement
			{
				Debug.Log("click");
				targetPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition); // Set target
				targetPosition.z = transform.position.z; // Reset z position to keep sprite from drifting behind camera.
				transform.position = targetPosition; // Move
			}
		}
	}
}