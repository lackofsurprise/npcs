﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerMovement : MonoBehaviour {
   
    public float moveSpeed;

	public Vector3 targetPosition;
   
    void Update () {
        if (Input.GetKeyDown(KeyCode.Mouse0))
		{
			// If mouse is over UI game object, don't register movement
			if (EventSystem.current.IsPointerOverGameObject()) return;
			{
				targetPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			
				// Reset z position to keep sprite from drifting behind camera.
				targetPosition.z = transform.position.z;
			}
			
		}
		transform.position = Vector3.MoveTowards(transform.position, targetPosition, Time.deltaTime * moveSpeed);
    }
}
