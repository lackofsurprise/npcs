﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

public class SaveController : MonoBehaviour {

	public static SaveController saveController;

	public static float health;
	public static float experience;

	// Singleton pattern to create one persistent object of this type.
	void Awake () {
		if (saveController == null)
		{
			// If no object of this type exists, don't destroy it on load.
			DontDestroyOnLoad(gameObject);
			
			// Set the static variable to this object.
			saveController = this;
		}
		
		else if (saveController != this)
		{
			// If this object isn't the one in public static control variable, destroy to prevent duplicates
			Destroy(gameObject);
		}
	}

	public void Save() {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create (Application.persistentDataPath + "/savedGames.gd");
        
		PlayerData data = new PlayerData();
		data.health = health;
		data.experience = experience;

		bf.Serialize(file, data);
        file.Close();

		Debug.Log("Saved");
	}

    public void Load() {
       
	    // Check there's a file to load
		if (File.Exists(Application.persistentDataPath + "/savedGames.gd"))
		{
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();

			// Loading saved values into variables
			health = data.health;
			experience = data.experience;

			Debug.Log("Loaded");
        }
		else
		{
			Debug.Log("Nothing to load");
		}
		
    }
}

[Serializable]
class PlayerData
{
	public float health;
	public float experience;
}
